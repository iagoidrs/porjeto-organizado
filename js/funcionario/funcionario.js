$(document).ready(function(){
	$("#btnGerarErro").click(function(){
		$.ajax({
			type: "post",
			url: "controller/funcionario/gerarErro.php",
			data: {},
			success: function(){
				$("#modalSucesso").modal("show");
			},
			error: function(){
				$("#modalErro").modal("show");
			}
		});
	});
	
	$("#btnCadastrarFuncionario").click(function(){
		$.ajax({
			type: "post",
			url: "controller/funcionario/funcionarioCadastro.php",
			data: {
				"nome": $("#txtNome").val()
			},
			success: function(data){
				
				switch($.trim(data)){
					case "nome_invalido":
						$("#modalNomeInvalido").modal("show");
					break;
					
					case "success":
						$("#modalSucessoCadastroFuncionario").modal("show");
					break;
					
					case "error":
					default:
						$("#modalErro").modal("show");
					break;
				}
			},
			error: function(){
				$("#modalErro").modal("show");
			}
		});
	});
});