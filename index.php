<!DOCTYPE html>
<html lang="en">
	<head>
		<meta charset="utf-8">
		<meta http-equiv="X-UA-Compatible" content="IE=edge">
		<meta name="viewport" content="width=device-width, initial-scale=1">
		<!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
		<title>Bootstrap 101 Template</title>

		<!-- Bootstrap -->
		<link href="css/bootstrap.min.css" rel="stylesheet">

		<!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
		<!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
		<!--[if lt IE 9]>
		<script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
		<script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
		<![endif]-->
	</head>
	
	<body>
		<h1>Hello, world!</h1>
		
		<button id="btnGerarErro" class="btn btn-danger">Gerar Erro!</button>
		
		<form action="javascript:void(0);">
			<div class="form-group">
				<label for="recipient-name" class="control-label">Nome:</label>
				<input class="form-control" id="txtNome">
			</div>
			
			<div class="form-group">
				<button id="btnCadastrarFuncionario" class="btn btn-success">Cadastrar</button>
			</div> 
		</form>
		
		
		
		<div id="modalSucessoCadastroFuncionario" class="modal fade" tabindex="-1" role="dialog">
			<div class="modal-dialog" role="document">
				<div class="modal-content">
					<div class="modal-header alert alert-success">
						<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
						<h4 class="modal-title">Funcionário cadastrado com sucesso</h4>
					</div>
					
					<div class="modal-body">
						<p class="text-success">O funcionário foi cadastrado com sucesso no sistema</p>
					</div>
					
					<div class="modal-footer">
						<button type="button" class="btn btn-default" data-dismiss="modal">Fechar</button>
					</div>
				</div><!-- /.modal-content -->
			</div><!-- /.modal-dialog -->
		</div><!-- /.modal -->
		
		<div id="modalNomeInvalido" class="modal fade" tabindex="-1" role="dialog">
			<div class="modal-dialog" role="document">
				<div class="modal-content">
					<div class="modal-header alert alert-warning">
						<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
						<h4 class="modal-title">Verifique os dados</h4>
					</div>
					
					<div class="modal-body">
						<p class="text-warning">O nome informado está inválido</p>
					</div>
					
					<div class="modal-footer">
						<button type="button" class="btn btn-default" data-dismiss="modal">Fechar</button>
					</div>
				</div><!-- /.modal-content -->
			</div><!-- /.modal-dialog -->
		</div><!-- /.modal -->
		
		<div id="modalErro" class="modal fade" tabindex="-1" role="dialog">
			<div class="modal-dialog" role="document">
				<div class="modal-content">
					<div class="modal-header alert alert-danger">
						<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
						<h4 class="modal-title">Erro inesperado</h4>
					</div>
					
					<div class="modal-body">
						<p class="text-danger">Erro inesperado! Entre em contato com a equipe de desenvolvimento.</p>
					</div>
					
					<div class="modal-footer">
						<button type="button" class="btn btn-default" data-dismiss="modal">Fechar</button>
					</div>
				</div><!-- /.modal-content -->
			</div><!-- /.modal-dialog -->
		</div><!-- /.modal -->
		
		
		
		
		<!-- OS SCRIPTS SÃO IMPORTADOS ABAIXO POR CAUSA DE PERFORMANCE --->
		
		
		
		<!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
		<script src="js/jquery-3.1.1.min.js"></script>
		
		<!-- Include all compiled plugins (below), or include individual files as needed -->
		<script src="js/bootstrap.min.js"></script>
		
		<!-- COMO ESTE ARQUIVO USA JQUERY, ELE TEM QUE SER IMPORTADO DEPOIS A INCLUSAO DA BIBLIOTECA (JQUERY) -->
		<script src="js/funcionario/funcionario.js"></script>
	</body>
</html>